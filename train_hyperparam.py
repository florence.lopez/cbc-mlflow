"""Trains a Keras model with hyperparameter search & MLflow.
Model code comes from here: https://github.com/tensorflow/serving """

import os 
import click
import math
import numpy as np
import pandas as pd
import mlflow
import shap

import matplotlib.pyplot as plt 
import matplotlib.dates as mdates

from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from mlflow.models.signature import infer_signature

DATA = 'training_data.csv'
PARAMS = {'n_estimators': 1500,
          'max_depth': 5, 
          'learning_rate': 0.05,
          'random_state': 0}
EXPERIMENT_NAME= 'hyperparam_hyperopt'

def eval_and_log_metrics(prefix, actual, pred, epoch):
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mlflow.log_metric("{}_rmse".format(prefix), rmse, step=epoch)
    return rmse


def build_creation_dates():    
    start_point= datetime.strptime('2019-07-01', "%Y-%m-%d")
    creation_dates=[]

    while start_point < datetime.strptime('2019-09-30', "%Y-%m-%d"):
        creation_dates.append(start_point)
        start_point=start_point+  relativedelta(months=+1)
    return creation_dates


def select_features(): 
    feature_select_werbespendings = ['euro_week_KINO', 'euro_weeks_1_KINO', 
                 
        'euro_week_OOH', 'euro_weeks_1_OOH', 'euro_weeks_2_OOH',
        
        'euro_week_PRINT', 'euro_weeks_1_PRINT', 'euro_weeks_2_PRINT',
                 
        'euro_week_RADIO', 'euro_weeks_1_RADIO', 'euro_weeks_2_RADIO',
                                 
        'euro_week_ONLINE', 'euro_weeks_1_ONLINE', 'euro_weeks_2_ONLINE', 'euro_weeks_3_ONLINE',
                 
        'euro_week_YOUTUBE', 'euro_weeks_1_YOUTUBE',  'euro_weeks_2_YOUTUBE', 'euro_weeks_3_YOUTUBE',
                                
                         
       'euro_week_TV_Week_NoPrime',
       'euro_week_TV_Week_Prime', 'euro_week_TV_Weekend_NoPrime',
       'euro_week_TV_Weekend_Prime', 'euro_weeks_1_TV_Week_NoPrime',
       'euro_weeks_1_TV_Week_Prime', 'euro_weeks_1_TV_Weekend_NoPrime',
       'euro_weeks_1_TV_Weekend_Prime', 'euro_weeks_2_TV_Week_NoPrime',
       'euro_weeks_2_TV_Week_Prime', 'euro_weeks_2_TV_Weekend_NoPrime',
       'euro_weeks_2_TV_Weekend_Prime', 'euro_weeks_3_TV_Week_NoPrime',
       'euro_weeks_3_TV_Week_Prime', 'euro_weeks_3_TV_Weekend_NoPrime',
       'euro_weeks_3_TV_Weekend_Prime']

    feature_select_baseline = ['month', 'year', 'brand_index','date_int']

    feature_select = feature_select_werbespendings+ feature_select_baseline

    return feature_select

@click.command(
    help="Trains an Keras model on wine-quality dataset."
    "The input is expected in csv format."
    "The model and its metrics are logged with mlflow."
)
@click.option("--epochs", type=click.INT, default=100, help="Maximum number of epochs to evaluate.")
@click.option(
    "--batch-size", type=click.INT, default=16, help="Batch size passed to the learning algo."
)
@click.option("--learning_rate", type=click.FLOAT, default=1e-2, help="Learning rate.")
@click.option("--seed", type=click.INT, default=97531, help="Seed for the random generator.")

def run(epochs, batch_size, learning_rate, seed):
    
    # load data
    nielsen_yougov_train = pd.read_csv(DATA, index_col=0)
    nielsen_yougov_train.date = pd.to_datetime(nielsen_yougov_train.date)

    # create list of dates: 
    creation_dates = build_creation_dates()

    # list of features taken into account
    feature_select = select_features()

    # create dataframe to save results
    result_df = pd.DataFrame()

    # build the model
    model = build_model()

    # set experiment name 
    mlflow.set_experiment(EXPERIMENT_NAME)

    for creation_date in creation_dates:
        with mlflow.start_run():

            train_data = nielsen_yougov_train[(nielsen_yougov_train.date >= (nielsen_yougov_train.date.min() +  timedelta(days=1*14))) &
                                            (nielsen_yougov_train.date < creation_date)].copy()
            test_data = nielsen_yougov_train[(nielsen_yougov_train.date >= creation_date) &
                                            (nielsen_yougov_train.date < creation_date + relativedelta(months=+1))].copy()
            
            X_train = train_data[feature_select]
            y_train = train_data['adaware']

            X_test = test_data[feature_select]
            y_test = test_data['adaware']
            

            if epochs == 0:  # score null model
                eval_and_log_metrics("train", y_train, np.ones(len(y_train)) * np.mean(y_train), epoch=-1)
                eval_and_log_metrics("test", y_test, np.ones(len(y_test)) * np.mean(y_test), epoch=-1)
            else:
                


if __name__ == "__main__":
    run()

