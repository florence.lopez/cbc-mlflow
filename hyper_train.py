import pandas as pd
import numpy as np
import mlflow
import shap

import matplotlib.pyplot as plt 
import matplotlib.dates as mdates

from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_squared_error
from sklearn import model_selection 

from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from mlflow.models.signature import infer_signature



DATA = 'training_data.csv'
PARAMS = {'n_estimators': 1500,
          'max_depth': 5, 
          'learning_rate': 0.05,
          'random_state': 0}
EXPERIMENT_NAME= 'hyper_train'
SEARCHSPACE = {
        'n_estimators': (1000, 2500),
        'max_depth': (1, 20),
        'learning_rate': (0.001, 0.1),
        'min_samples_split': (1,100),
        'min_samples_leaf': (1,100)
    }


def build_creation_dates():    
    start_point= datetime.strptime('2019-07-01', "%Y-%m-%d")
    creation_dates=[]

    while start_point < datetime.strptime('2019-09-30', "%Y-%m-%d"):
        creation_dates.append(start_point)
        start_point=start_point+  relativedelta(months=+1)
    return creation_dates


def select_features(): 
    feature_select_werbespendings = ['euro_week_KINO', 'euro_weeks_1_KINO', 
                 
        'euro_week_OOH', 'euro_weeks_1_OOH', 'euro_weeks_2_OOH',
        
        'euro_week_PRINT', 'euro_weeks_1_PRINT', 'euro_weeks_2_PRINT',
                 
        'euro_week_RADIO', 'euro_weeks_1_RADIO', 'euro_weeks_2_RADIO',
                                 
        'euro_week_ONLINE', 'euro_weeks_1_ONLINE', 'euro_weeks_2_ONLINE', 'euro_weeks_3_ONLINE',
                 
        'euro_week_YOUTUBE', 'euro_weeks_1_YOUTUBE',  'euro_weeks_2_YOUTUBE', 'euro_weeks_3_YOUTUBE',
                                
                         
       'euro_week_TV_Week_NoPrime',
       'euro_week_TV_Week_Prime', 'euro_week_TV_Weekend_NoPrime',
       'euro_week_TV_Weekend_Prime', 'euro_weeks_1_TV_Week_NoPrime',
       'euro_weeks_1_TV_Week_Prime', 'euro_weeks_1_TV_Weekend_NoPrime',
       'euro_weeks_1_TV_Weekend_Prime', 'euro_weeks_2_TV_Week_NoPrime',
       'euro_weeks_2_TV_Week_Prime', 'euro_weeks_2_TV_Weekend_NoPrime',
       'euro_weeks_2_TV_Weekend_Prime', 'euro_weeks_3_TV_Week_NoPrime',
       'euro_weeks_3_TV_Week_Prime', 'euro_weeks_3_TV_Weekend_NoPrime',
       'euro_weeks_3_TV_Weekend_Prime']

    feature_select_baseline = ['month', 'year', 'brand_index','date_int']

    feature_select = feature_select_werbespendings+ feature_select_baseline

    return feature_select

def build_model(): 

    model = GradientBoostingRegressor(**PARAMS)

    return model 

def print_learning_curve(label, model, X_train, y_train, X_test, y_test):

    test_score = np.zeros((PARAMS['n_estimators'],), dtype=np.float64)
    for i, y_pred_test in enumerate(model.staged_predict(X_test)):
        score = np.sqrt(mean_squared_error(y_test, y_pred_test))
        test_score[i] = score
        #mlflow.log_metric('test_score', score)
        
    train_score = np.zeros((PARAMS['n_estimators'],), dtype=np.float64)
    for i, y_pred_train in enumerate(model.staged_predict(X_train)):
        score = np.sqrt(mean_squared_error(y_train, y_pred_train))
        train_score[i] = score
        #mlflow.log_metric('train_score', score)
    
    fig = plt.figure(figsize=(6, 6))
    plt.subplot(1, 1, 1)
    plt.title('Learning Curve')
    plt.plot(np.arange(PARAMS['n_estimators']) + 1, train_score, 'b-',
             label='Training Set')
    plt.plot(np.arange(PARAMS['n_estimators']) + 1, test_score, 'r-',
             label='Test Set')
    plt.legend(loc='upper right')
    plt.xlabel('Boosting Iterations')
    plt.ylabel('RMSE')
    fig.tight_layout()
    save_name = label + '_plot.png'
    fig.savefig(save_name)
    mlflow.log_artifact(save_name)

def log_model(model, train_X):
    signature = infer_signature(train_X, model.predict(train_X))
    mlflow.sklearn.log_model(model, "gradient_boosting_regressor", signature=signature)
    
def regression_metrics(actual, pred):
    return {
        "MAE": metrics.mean_absolute_error(actual, pred),
        "RMSE": np.sqrt(metrics.mean_squared_error(actual, pred))}

def train():
    # load data
    nielsen_yougov_train = pd.read_csv(DATA, index_col=0)
    nielsen_yougov_train.date = pd.to_datetime(nielsen_yougov_train.date)

    # create list of dates: 
    creation_dates = build_creation_dates()

    # list of features taken into account
    feature_select = select_features()

    # create dataframe to save results
    result_df = pd.DataFrame()

    # build the model
    model = build_model()

    # set experiment name 
    mlflow.set_experiment(EXPERIMENT_NAME)

    # enable auto-logging
    # mlflow.sklearn.autolog()
    for creation_date in creation_dates:

        with mlflow.start_run() as run:
        # Fit CV models; extract predictions and metrics

            print(type(PARAMS))
            print(PARAMS)

            train_data = nielsen_yougov_train[(nielsen_yougov_train.date >= (nielsen_yougov_train.date.min() +  timedelta(days=1*14))) &
                                                (nielsen_yougov_train.date < creation_date)].copy()
            test_data = nielsen_yougov_train[(nielsen_yougov_train.date >= creation_date) &
                                            (nielsen_yougov_train.date < creation_date + relativedelta(months=+1))].copy()
            
            X_train = train_data[feature_select]
            y_train = train_data['adaware']

            X_test = test_data[feature_select]
            y_test = test_data['adaware']

            model_cv = GradientBoostingRegressor(**PARAMS)
            y_pred_cv = model_selection.cross_val_predict(model_cv, X_train, y_train)
            #metrics_cv = {f"val_{metric}": value for metric, value in regression_metrics(y_train, y_pred_cv).items()}

            # Fit and log full training sample model; extract predictions and metrics
            mlflow.sklearn.autolog()
            model = GradientBoostingRegressor(**PARAMS)
            model.fit(X_train, y_train)

            y_pred_test = model.predict(x_test)
            metrics_test = {
            f"test_{metric}": value
            for metric, value in regression_metrics(y_test, y_pred_test).items()}
            
            metrics = {**metrics_test, **metrics_cv}
            mlflow.log_metrics(metrics)


if __name__ == "__main__":
    train()