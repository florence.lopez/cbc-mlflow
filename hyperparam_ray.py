import pandas as pd
import numpy as np
import mlflow
import shap
import time

import matplotlib.pyplot as plt 
import matplotlib.dates as mdates

from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_squared_error, r2_score
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta

from mlflow.models.signature import infer_signature
from skopt import BayesSearchCV

from tune_sklearn import TuneSearchCV



DATA = 'training_data.csv'
EXPERIMENT_NAME= 'hyperparam_ray3'
BASELINE_PARAMS = {'n_estimators': 1500,
          'max_depth': 5, 
          'learning_rate': 0.05,
          'random_state': 0}
SEARCHSPACE = {
        #'n_estimators': (10, 100),
        'max_depth': (1, 50),
        'learning_rate': (0.01, 0.1),
        'min_samples_split': (1,100),
        'min_samples_leaf': (1,100)
    }
N_CALLS = 2


def build_creation_dates():    
    start_point= datetime.strptime('2019-07-01', "%Y-%m-%d")
    creation_dates=[]

    while start_point < datetime.strptime('2019-09-30', "%Y-%m-%d"):
        creation_dates.append(start_point)
        start_point=start_point+  relativedelta(months=+1)
    return creation_dates


def select_features(): 
    feature_select_werbespendings = ['euro_week_KINO', 'euro_weeks_1_KINO', 
                 
        'euro_week_OOH', 'euro_weeks_1_OOH', 'euro_weeks_2_OOH',
        
        'euro_week_PRINT', 'euro_weeks_1_PRINT', 'euro_weeks_2_PRINT',
                 
        'euro_week_RADIO', 'euro_weeks_1_RADIO', 'euro_weeks_2_RADIO',
                                 
        'euro_week_ONLINE', 'euro_weeks_1_ONLINE', 'euro_weeks_2_ONLINE', 'euro_weeks_3_ONLINE',
                 
        'euro_week_YOUTUBE', 'euro_weeks_1_YOUTUBE',  'euro_weeks_2_YOUTUBE', 'euro_weeks_3_YOUTUBE',
                                
                         
       'euro_week_TV_Week_NoPrime',
       'euro_week_TV_Week_Prime', 'euro_week_TV_Weekend_NoPrime',
       'euro_week_TV_Weekend_Prime', 'euro_weeks_1_TV_Week_NoPrime',
       'euro_weeks_1_TV_Week_Prime', 'euro_weeks_1_TV_Weekend_NoPrime',
       'euro_weeks_1_TV_Weekend_Prime', 'euro_weeks_2_TV_Week_NoPrime',
       'euro_weeks_2_TV_Week_Prime', 'euro_weeks_2_TV_Weekend_NoPrime',
       'euro_weeks_2_TV_Weekend_Prime', 'euro_weeks_3_TV_Week_NoPrime',
       'euro_weeks_3_TV_Week_Prime', 'euro_weeks_3_TV_Weekend_NoPrime',
       'euro_weeks_3_TV_Weekend_Prime']

    feature_select_baseline = ['month', 'year', 'brand_index','date_int']

    feature_select = feature_select_werbespendings+ feature_select_baseline

    return feature_select

def print_learning_curve(label, model, X_train, y_train, X_test, y_test, params):

    test_score = np.zeros((params['n_estimators'],), dtype=np.float64)
    for i, y_pred_test in enumerate(model.staged_predict(X_test)):
        score = np.sqrt(mean_squared_error(y_test, y_pred_test))
        test_score[i] = score
        #mlflow.log_metric('test_score', score)
        
    train_score = np.zeros((params['n_estimators'],), dtype=np.float64)
    for i, y_pred_train in enumerate(model.staged_predict(X_train)):
        score = np.sqrt(mean_squared_error(y_train, y_pred_train))
        train_score[i] = score
        #mlflow.log_metric('train_score', score)
    
    fig = plt.figure(figsize=(6, 6))
    plt.subplot(1, 1, 1)
    plt.title('Learning Curve')
    plt.plot(np.arange(params['n_estimators']) + 1, train_score, 'b-',
             label='Training Set')
    plt.plot(np.arange(params['n_estimators']) + 1, test_score, 'r-',
             label='Test Set')
    plt.legend(loc='upper right')
    plt.xlabel('Boosting Iterations')
    plt.ylabel('RMSE')
    fig.tight_layout()
    save_name = label + '_plot.png'
    fig.savefig(save_name)
    mlflow.log_artifact(save_name)


def train():

    # load data
    nielsen_yougov_train = pd.read_csv(DATA, index_col=0)
    nielsen_yougov_train.date = pd.to_datetime(nielsen_yougov_train.date)

    # create list of dates: 
    creation_dates = build_creation_dates()

    # list of features taken into account
    feature_select = select_features()

    # set experiment name 
    mlflow.set_experiment(EXPERIMENT_NAME)

    for creation_date in creation_dates:
        with mlflow.start_run() as run:

            train_data = nielsen_yougov_train[(nielsen_yougov_train.date >= (nielsen_yougov_train.date.min() +  timedelta(days=1*14))) &
                                                (nielsen_yougov_train.date < creation_date)].copy()
            test_data = nielsen_yougov_train[(nielsen_yougov_train.date >= creation_date) &
                                            (nielsen_yougov_train.date < creation_date + relativedelta(months=+1))].copy()
            
            X_train = train_data[feature_select]
            y_train = train_data['adaware']

            X_test = test_data[feature_select]
            y_test = test_data['adaware']

            model = GradientBoostingRegressor()

            mlflow.sklearn.autolog()

            tune_search = TuneSearchCV(estimator=model, param_distributions=SEARCHSPACE, n_trials=5, early_stopping=True,
            max_iters=10, search_optimization="bayesian", loggers=["mlflow"], verbose=2)

            tune_search.fit(X_train, y_train)

            print(tune_search.best_params_)


if __name__ == "__main__":
    train()